package com.learn.Ecommerce.service;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.learn.Ecommerce.dto.ProductDto;
import com.learn.Ecommerce.dto.ResponseDto;
import com.learn.Ecommerce.model.Product;
import com.learn.Ecommerce.repository.ReviewRepository;

import junit.framework.Assert;

@RunWith(MockitoJUnitRunner.Silent.class)
public class ReviewServiceImplTest {

	@InjectMocks
	ReviewServiceImpl reviewServiceImpl;

	@Mock
	ReviewRepository reviewRepository;

	@Test
	public void testreviewForPositive() {
		List<Product> products = new ArrayList();
		Product dto = new Product();
		dto.setId(1);
		dto.setName("booapl");
		dto.setPrice(null);
		products.add(dto);
		Mockito.when(reviewRepository.findById(1)).thenReturn(null);
		ResponseDto produ =  reviewServiceImpl.review(null);
		Assert.assertNotNull(produ);
		Assert.assertEquals(1, ((List<ProductDto>) produ).size());
 	}

	@Test
	public void testreviewForNagative() {
		List<Product> products = new ArrayList();
		Product dto = new Product();
		dto.setId(-1);
		dto.setName("booapl");
		dto.setPrice(null);
		products.add(dto);
		Mockito.when(reviewRepository.findById(1)).thenReturn(null);
		ResponseDto produ =  reviewServiceImpl.review(null);
		Assert.assertNotNull(produ);
		Assert.assertEquals(1, ((List<ProductDto>) produ).size());
 	}
}
