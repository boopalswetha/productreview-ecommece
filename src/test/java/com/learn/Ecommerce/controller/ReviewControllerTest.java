package com.learn.Ecommerce.controller;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.learn.Ecommerce.dto.ProductDto;
import com.learn.Ecommerce.dto.ResponseDto;
import com.learn.Ecommerce.service.ReviewServiceImpl;

import junit.framework.Assert;

@RunWith(MockitoJUnitRunner.Silent.class)
public class ReviewControllerTest {

	@InjectMocks
	ReviewController sreviewController;

	@Mock
	ReviewServiceImpl  reviewServiceImpl;

	@Test
	public void testreviewForPositive() {
		List<ProductDto> review = new ArrayList();
		ProductDto productDto = new ProductDto();
		productDto.setId(1);
		productDto.setFeedBack("good");
		productDto.setRating(2.1);
		ResponseDto responseDto = new ResponseDto();
		Mockito.when(reviewServiceImpl.review(productDto)).thenReturn(responseDto);
		ResponseEntity<ResponseDto> boopal = sreviewController.review(productDto);
		Assert.assertNotNull(boopal);
		Assert.assertEquals(boopal.getStatusCode(), HttpStatus.OK);
	}
	@Test
	public void testreviewForNagative() {
		List<ProductDto> review = new ArrayList();
		ProductDto productDto = new ProductDto();
		productDto.setId(-1);
		productDto.setFeedBack("good");
		productDto.setRating(2.1);
		ResponseDto responseDto = new ResponseDto();
		Mockito.when(reviewServiceImpl.review(productDto)).thenReturn(responseDto);
		ResponseEntity<ResponseDto> boopal = sreviewController.review(productDto);
		Assert.assertNotNull(boopal);
		Assert.assertEquals(boopal.getStatusCode(), HttpStatus.OK);
	}
}
