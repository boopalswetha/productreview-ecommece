package com.learn.Ecommerce.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@Entity
@Table
public class ProductReview {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer productReviewId;
    private double rating;
	private String feedBack;
	@ManyToOne( cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Product product;
	
	@JsonIgnoreProperties
	@ManyToOne( cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private User user;
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Integer getProductReviewId() {
		return productReviewId;
	}
	public void setProductReviewId(Integer productReviewId) {
		this.productReviewId = productReviewId;
	}
	public double getRating() {
		return rating;
	}
	public void setRating(double rating) {
		this.rating = rating;
	}
	public String getFeedBack() {
		return feedBack;
	}
	public void setFeedBack(String feedBack) {
		this.feedBack = feedBack;
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	

}
