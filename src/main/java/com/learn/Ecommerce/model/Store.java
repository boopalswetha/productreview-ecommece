package com.learn.Ecommerce.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table
public class Store {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer storeId;

	private String storeName;
	
	@ManyToOne( cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Product product;
	
	@OneToMany( cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<StoreReview> StoreReview;

	public Integer getStoreId() {
		return storeId;
	}

	public void setStoreId(Integer storeId) {
		this.storeId = storeId;
	}

	public String getStoreName() {
		return storeName;
	}

	public List<StoreReview> getStoreReview() {
		return StoreReview;
	}

	public void setStoreReview(List<StoreReview> storeReview) {
		StoreReview = storeReview;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	
}
