package com.learn.Ecommerce.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.learn.Ecommerce.model.Product;

public interface ProductRepository extends JpaRepository<Product, Integer> {

	List<Product> findByNameLike(String productName);

}
