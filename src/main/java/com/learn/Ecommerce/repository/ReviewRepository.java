package com.learn.Ecommerce.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.learn.Ecommerce.model.Product;
import com.learn.Ecommerce.model.ProductReview;

@Repository
public interface ReviewRepository extends JpaRepository<ProductReview, Integer>{

	public Product findByProduct(Product product);


	public List<ProductReview> findRatingByProductId(Object setId);


	public List<ProductReview> findByRating(int id);


	public List<ProductReview> findByProductId(int i);


	public List<ProductReview> findByProductId(Product product);

	
}
