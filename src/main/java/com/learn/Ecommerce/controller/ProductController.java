package com.learn.Ecommerce.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.learn.Ecommerce.exceptions.ProductNameNotFoundException;
import com.learn.Ecommerce.model.Product;
import com.learn.Ecommerce.service.ProductService;

@RestController
public class ProductController {
	@Autowired

	ProductService productService;

	@GetMapping("/products/{productName}")

	public ResponseEntity<List<Product>> viewProductName(@PathVariable String productName)
			throws ProductNameNotFoundException {

		List<Product> products = productService.viewProductByName(productName);

		return new ResponseEntity<List<Product>>(products, HttpStatus.OK);

	}

}
