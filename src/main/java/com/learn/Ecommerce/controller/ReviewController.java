package com.learn.Ecommerce.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.learn.Ecommerce.dto.ProductDto;
import com.learn.Ecommerce.dto.ResponseDto;
import com.learn.Ecommerce.exceptions.ProductNotFoundException;
import com.learn.Ecommerce.model.ProductReview;
import com.learn.Ecommerce.service.ReviewService;

@RestController
public class ReviewController {

	@Autowired
	ReviewService reviewService;
	
	@PostMapping(value="/review")
	public ResponseEntity<ResponseDto> review(@RequestBody ProductDto productDto) throws ProductNotFoundException {
		ResponseDto responseDto = reviewService.review(productDto);
		return new ResponseEntity<>(responseDto,HttpStatus.OK);
	}
	@GetMapping(value="/reating/{id}")
	public ResponseEntity<ResponseDto> getrating(@PathVariable("id") int id) {
		double responseDto = reviewService.getrating(id);
		return new ResponseEntity<ResponseDto>(HttpStatus.OK);
	}
}