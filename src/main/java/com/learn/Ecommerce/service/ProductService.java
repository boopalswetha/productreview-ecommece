package com.learn.Ecommerce.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.learn.Ecommerce.exceptions.ProductNameNotFoundException;
import com.learn.Ecommerce.model.Product;
import com.learn.Ecommerce.repository.ProductRepository;

@Service
public class ProductService {
	@Autowired

	ProductRepository productRepository;

	public List<Product> viewProductByName(String productName) throws ProductNameNotFoundException {

		List<Product> products = productRepository.findByNameLike("%" + productName + "%");

		if (products.isEmpty()) {

			throw new ProductNameNotFoundException(productName);

		} else {

			return products;

		}

	}

}
