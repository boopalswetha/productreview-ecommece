package com.learn.Ecommerce.service;

import com.learn.Ecommerce.dto.ProductDto;
import com.learn.Ecommerce.dto.ResponseDto;
import com.learn.Ecommerce.exceptions.ProductNotFoundException;
import com.learn.Ecommerce.model.Product;
import com.learn.Ecommerce.model.ProductReview;

public interface ReviewService {

	//public ResponseDto review(Product product) throws ProductNotFoundException;

	//public ResponseDto review(ProductDto productDto);

	//public ResponseDto review(ProductReview productreview) throws ProductNotFoundException;

	public ResponseDto review(ProductDto productDto) throws ProductNotFoundException;

	public double getrating(int id);

}
