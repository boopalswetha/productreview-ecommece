package com.learn.Ecommerce.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.learn.Ecommerce.dto.ProductDto;
import com.learn.Ecommerce.dto.ResponseDto;
import com.learn.Ecommerce.exceptions.ProductNotFoundException;
import com.learn.Ecommerce.exceptions.UsersNotFoundException;
import com.learn.Ecommerce.model.Product;
import com.learn.Ecommerce.model.ProductReview;
import com.learn.Ecommerce.model.User;
import com.learn.Ecommerce.repository.ProductRepository;
import com.learn.Ecommerce.repository.ReviewRepository;
import com.learn.Ecommerce.repository.UserRepository;

@Service
public class ReviewServiceImpl implements ReviewService {

	@Autowired
	ReviewRepository reviewRepository;
	@Autowired
	UserRepository userRepository;
	@Autowired
	ProductRepository productRepository;

	@Override
	public ResponseDto review(ProductDto productDto) throws ProductNotFoundException {
		Optional<Product> optional = productRepository.findById(productDto.getId());
		List<ProductReview> avg =reviewRepository.findByProductId(1);
		
        double sum =0;
        double average= 0;
        
		for(int i=0;i<avg.size();i++) {
			sum=sum+avg.get(i).getRating();
		}
		average=sum/avg.size();
		System.out.println(average);  
		
		
		Product product = null;
		if (optional.isPresent()) {
			product = optional.get();
		} else {
			throw new ProductNotFoundException("product not found");
		}
		Optional<User> optionals = userRepository.findById(productDto.getUserId());
		User user = null;
		if (optionals.isPresent()) {
			user = optionals.get();
		} else {
			throw new UsersNotFoundException("user is not found");

		}

		User users = new User();
		users.setUserId(productDto.getUserId());

		ProductReview productReview = new ProductReview();
		productReview.setFeedBack(productDto.getFeedBack());
		productReview.setProduct(product);
		productReview.setRating(productDto.getRating());
		productReview.setUser(user);
		userRepository.save(user);

		ProductReview productreview;
		reviewRepository.save(productReview);

		ResponseDto dto = new ResponseDto();
		dto.setMessage("rating given sucessfully");
		return dto;
	}

	@Override
	public double getrating(int id) {
	List<ProductReview> avg =reviewRepository.findByProductId(id);
	 double sum =0;
     double average= 0;
     
		for(int i=0;i<avg.size();i++) {
			sum=sum+avg.get(i).getRating();
		}
		average=sum/avg.size();
		System.out.println(average);
		//return average;
		return average;
	}
	
}